'use stricts'

let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url= 'http://localhost:3000';


describe('INSERTAR UN PAIS: ',()=>{

	it('DEBERIA INSERTAR UN PAIS', (done) => {
		chai.request(url)
			.post('/country')
			.send({id:0, country: "Salvador", year: 2017, days: 10})
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);//OJO CON EL RES MENSAJE Y CODIGO O ESTADO
				done();
			});
	});
});

describe('INSERTAR PAIS CON ERROR: ',()=>{

	it('DEBERIA RECIBIR UN ERROR', (done) => {
		chai.request(url)
			.post('/country')
			.send({id:1, country: "Coban", year: 2010, days: 10})
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(500);//OJO CON EL RES MENSAJE Y CODIGO O ESTADO
				done();
			});
	});

});

describe('OBTENER PAIS: ',()=>{

	it('DEBERIA TENER UN PAIS', (done) => {
		chai.request(url)
			.get('/countries')
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);//OJO CON EL RES MENSAJE Y CODIGO O ESTADO
				done();
			});
	});

});

describe('OBTENEMOS UN PAIS CON EL ID 1: ',()=>{

	it('DEBERIAMOS OBTENER EL PAIS CON EL ID 1', (done) => {
		chai.request(url)
			.get('/country/1')
			.end( function(err,res){
				console.log(res.body)
				expect(res.body).to.have.property('id').to.be.equal(1); //VALIDAMOS EL CUERPO QUE SE MANDO SI NO TRUENA
				expect(res).to.have.status(200); //OJO CON EL RES MENSAJE Y CODIGO O ESTADO
				done();
			});
	});

});

describe('ACTUALIACION CON EL ID  1: ',()=>{

	it('DEBERIA ACTUALIZAR EL NUMERO DE DIAS', (done) => {
		chai.request(url)
			.put('/country/1/days/20')
			.end( function(err,res){
				console.log(res.body)
				expect(res.body).to.have.property('days').to.be.equal(20);//VALIDAMOS EL CUERPO QUE SE MANDO SI NO TRUENA
				expect(res).to.have.status(200); //OJO CON EL RES MENSAJE Y CODIGO O ESTADO
				done();
			});
	});

});


//EN CONCATENACION
describe('ELIMINAMOS EL PAIS CON EL ID 1 ',()=>{

	it('DEBERIA ELIMINAR EL PAIES CON EL ID 1 1', (done) => {
		chai.request(url)
			.get('/countries')
			.end( function(err,res){
				console.log(res.body)
				expect(res.body).to.have.lengthOf(2); 
				expect(res).to.have.status(200);//VALIDAMOAS EL ESTADO
				chai.request(url)
					.del('/country/1') //OBTENEMOS LOS PAISES CON EL ID 1 NO DEBERIA EXISTIR
					.end( function(err,res){
						console.log(res.body)
						expect(res).to.have.status(200);
						chai.request(url)
							.get('/countries') //OBTENEMOS TODOS LOS PAISES
							.end( function(err,res){
								console.log(res.body)
								expect(res.body).to.have.lengthOf(1);
								expect(res.body[0]).to.have.property('id').to.be.equal(0);
								expect(res).to.have.status(200);
								done();
						});
					});
			});
	});

});


//INSERCION DE OTRA FORMATO INVALIDO ERROR NO ES JSON
describe('INSERTAR CON FORM: ',()=>{

	it('DEBERIA RECBIR UN ERROR DADO QUE SE ESTA INSERTANDO CON UN FORMATO INVALIDO', (done) => {
		chai.request(url)
			.post('/country')
			.type('form')
			.send({id:0, country: "Croacia", year: 2017, days: 10})
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(500);
				done();
			});
	});
});



//TEMPORAL................
// The `agent` now has the sessionid cookie saved, and will send it
// back to the server in the next request:
var agent = chai.request.agent(url)
describe('Authenticate a user: ',()=>{

	it('should receive an OK and a cookie with the authentication token', (done) => {
		agent
			.get('/authentication')
  			.auth('user', 'password')
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.cookie('authToken');
				expect(res).to.have.status(200);
				return agent.get('/personalData/user')
      			.then(function (res) {
         			expect(res).to.have.status(200);
         			console.log(res.body)
         			done();
      			});
				done();
			});
	});

});

describe('Obtain personal data without authToken: ',()=>{

	it('should receive an error because we need authToken', (done) => {
		agent
			.get('/personalData/user')
      		.then(function (res) {
         		expect(res).to.have.status(500);
         		console.log(res.body)
      	});
		done();
	});

});
