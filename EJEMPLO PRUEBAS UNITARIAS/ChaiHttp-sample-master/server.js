'use stricts';

const restify = require('restify');
var CookieParser = require('restify-cookies');
const server = restify.createServer();
server.use(restify.jsonBodyParser({ mapParams: true }))
server.use(CookieParser.parse);


server.get('/', function(req, res) {  
   res.send("ANALISIS Y DISEÑO 1");
});
server.use(restify.CORS());


//CREAMOS OBJETOS O VARIABLES PAIS Y USUARIO
let pais={id: 0,country:"Guatemala",year:2015,days:17};
let usuario={id: 0,user:"user",name:"Cesar",age:"25"};


//VREAMOS UN DICCIONARIO UN ARREGLO CON OBJETOS DE TIPO PAIS
var countries = [pais];
let lastId=0;


//INGRESAMOS UN NUEVO PAIS
server.post('/country',(req,res,next)=>{
	const body=req.body; //OBTENEMOS LOS DATOS
	console.log("---------------------------");
	console.log(body);
	console.log("---------------------------");
	//VALIDAMOS QUE SEA UN PAIS
	if(body.country==='Coban'){
		res.send(500,'Elemento country enviado no es un país'); //OJO CON EL RES MENSAJE Y CODIGO O ESTADO
	}else if((JSON.stringify(body)).includes('&')){//VALIDACION QUE TENGA JSON
		res.send(500,'Elemento viene como un formulario. Solo se admite JSON');
	}else{//TODO CORRECTO INSERTAMOS EN EL DICCIONARIO
		body.id=++lastId;
		countries.push(body);
		res.send(200,'Se ha introducido país'); //OJO CON EL RES MENSAJE Y CODIGO O ESTADO
	}
	next();
});


//OBTENEMOS TODOS LOS PAISES
server.get('/countries',(req,res,next)=>{
	
	res.send(200,countries);//OJO CON EL RES MENSAJE Y CODIGO O ESTADO
	next();
});


//OBTENEMOS UN PAIS EN ESPECIFICO
server.get('/country/:id',(req,res,next)=>{
	var id=req.params.id;

	res.send(200,countries[id]);//OJO CON EL RES MENSAJE Y CODIGO O ESTADO
	next();
});

//ELIMINAMOS UN PAIS EN EL ARREGLO
server.del('/country/:id',(req,res,next)=>{
	var id=req.params.id;
	//delete countries[id];
	countries.splice(id,1); //ELIMINACION
	res.send(200,'Elemento eliminado');//OJO CON EL RES MENSAJE Y CODIGO O ESTADO
	next();
});


//ACTUALIAMOS UN PAIS
server.put('/country/:id/days/:days',(req,res,next)=>{
	var id=req.params.id;
	var days=parseInt(req.params.days);
	var data=countries[id];
	data.days=days;
	countries.splice(id,1,data);
	console.log(countries);
	res.send(200,data);//OJO CON EL RES MENSAJE Y CODIGO O ESTADO
	next();
});

//TEMPORAL................
//REGRESAMOS UN TOQUEN DE VALIDACION  FICTICIO
server.get('/authentication',(req,res,next)=>{
	const body=req.body;
	console.log(req.authorization);
	res.setCookie('authToken','Token');
	res.send(200,"El usuario se encuentra en la BBDD");//OJO CON EL RES MENSAJE Y CODIGO O ESTADO
	next();
});


//VERIFIACION DE TOKEN DE AUTENTICACION
server.get('/personalData/:user',(req,res,next)=>{
	var user=req.params;
	var cookies=req.cookies;
	console.log(user);
	console.log(cookies);
	if(cookies.authToken==="Token"){
		res.send(200,usuario);//OJO CON EL RES MENSAJE Y CODIGO O ESTADO
	}else{
		res.send(500,"Se necesita el token de autenticación");//OJO CON EL RES MENSAJE Y CODIGO O ESTADO
	}
	next();
});

///notes/:id--> req.params.id
server.listen(3000, function() {
    console.log("Node server running on http://localhost:3000");
  });
